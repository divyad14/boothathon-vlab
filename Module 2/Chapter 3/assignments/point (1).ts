function check()
{
let a: HTMLInputElement = <HTMLInputElement>document.getElementById("x1");
let b: HTMLInputElement = <HTMLInputElement>document.getElementById("y1");
let c: HTMLInputElement = <HTMLInputElement>document.getElementById("x2");
let d: HTMLInputElement = <HTMLInputElement>document.getElementById("y2");
let e: HTMLInputElement = <HTMLInputElement>document.getElementById("x3");
let f: HTMLInputElement = <HTMLInputElement>document.getElementById("y3");
let g: HTMLInputElement = <HTMLInputElement>document.getElementById("x");
let h: HTMLInputElement = <HTMLInputElement>document.getElementById("y");
let answer: HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("answer");

var x1 : number=parseFloat(a.value); //WE ASSIGN INPUT VALUES TO THE VARIABLE
var y1: number=parseFloat(b.value);    
var x2: number=parseFloat(c.value);  
var y2: number=parseFloat(d.value); 
var x3: number=parseFloat(e.value); 
var y3: number=parseFloat(f.value); 
var x: number=parseFloat(g.value);  
var y : number=parseFloat(h.value); 

var p1=Math.abs((x*(y1-y2)+x1*(y2-y)+x2*(y-y1))/2);  //we apply the formula of area of triangle
var p2=Math.abs((x*(y2-y3)+ x2*(y3-y)+x3*(y-y2))/2); //we take absolute values as area cant be zero
var p3=Math.abs((x*(y3-y1)+x3*(y1-y)+x1*(y-y3))/2);  

var p=Math.abs((x1*(y2-y3)+x2*(y3-y1)+x3*(y1-y2))/2); 
var z =Math.abs(p1+p2+p3); 
console.log(p);
console.log(z);
var q= Math.abs(p-z);
console.log(q);
if(q<0.000001)    //we check if ans is smaller than 0.000001 and if yes then print the answer
{
    document.getElementById("answer").innerHTML = "The point lies inside the triangle.";
}
else
{
    document.getElementById("answer").innerHTML = "The point lies outside the triangle.";
}
}
